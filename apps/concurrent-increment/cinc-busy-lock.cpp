#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <string.h>

#define ARRAY_SIZE 1024
#define ARRAY_ALLOC_SIZE (ARRAY_SIZE * sizeof(uint64_t))
#define THREAD_COUNT 8

static uint64_t *A;

static std::mutex m;

static void
increment_thread()
{
  for (int i = 0; i < ARRAY_SIZE; i++) {
    m.lock();
    A[i]++;
    m.unlock();
  }
}

static void
increment()
{
  std::thread *threads[THREAD_COUNT];

  for (int i = 0; i < THREAD_COUNT; i++)
    threads[i] = new std::thread(increment_thread);

  for (int i = 0; i < THREAD_COUNT; i++)
    threads[i]->join();
}

static int
check_array(uint64_t expected_value)
{
  for (int i = 0; i < ARRAY_SIZE; i++) {
    if (A[i] != expected_value)
      return i;
  }
  return -1;
}

int
main(int argc, const char *argv[])
{
  A = (uint64_t *) malloc(ARRAY_ALLOC_SIZE);
  memset(A, 0, ARRAY_ALLOC_SIZE);

  increment_thread(); /* warmup omits translation time from the threaded loop */
  memset(A, 0, ARRAY_ALLOC_SIZE);

  increment();

  int invalid_index = check_array(THREAD_COUNT);
  if (invalid_index < 0) {
    printf("Success! All array entries contain the expected value %d\n", THREAD_COUNT);
  } else {
    printf("Error: array contains %lld at index %d, but value %lld was expected\n", A[invalid_index], invalid_index, THREAD_COUNT);
    return 1;
  }

  return 0;
}
