#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <string.h>

#define ARRAY_SIZE 1024
#define ARRAY_ALLOC_SIZE (ARRAY_SIZE * sizeof(uint64_t))
#define THREAD_COUNT 8

static uint64_t *A;

static std::mutex m;

static void
increment()
{
  for (int i = 0; i < ARRAY_SIZE; i++)
    A[i]++;
}

static void
loop_safe_increment()
{
  m.lock();
  for (int i = 0; i < ARRAY_SIZE; i++)
    A[i]++;
  m.unlock();
}

static void
slot_safe_increment()
{
  for (int i = 0; i < ARRAY_SIZE; i++) {
    m.lock();
    A[i]++;
    m.unlock();
  }
}

static int
check_array(uint64_t expected_value)
{
  for (int i = 0; i < ARRAY_SIZE; i++) {
    if (A[i] != expected_value)
      return (i+1);
  }
  return 0;
}

int
main(int argc, const char *argv[])
{
  A = (uint64_t *) malloc(ARRAY_ALLOC_SIZE);
  memset(A, 0, ARRAY_ALLOC_SIZE);

  void (*loop)(void) = NULL;
  if (argc > 1) {
    switch (argv[1][0]) {
      case 's':
        printf("Using slot-safe increment\n");
        loop = slot_safe_increment; break;
      case 'l':
        printf("Using loop-safe increment\n");
        loop = loop_safe_increment; break;
    }
  }
  if (loop == NULL) {
    loop = increment;
    printf("Using unsafe increment\n");
  }

  std::thread *threads[THREAD_COUNT];

  auto start = std::chrono::steady_clock::now();

  for (int i = 0; i < THREAD_COUNT; i++)
    threads[i] = new std::thread(loop);

  for (int i = 0; i < THREAD_COUNT; i++)
    threads[i]->join();

  auto duration = (std::chrono::steady_clock::now() - start);

  printf("Duration is %lld\n", duration);

  int invalid_index = check_array(THREAD_COUNT);
  if (invalid_index == 0) {
    printf("Success! All array entries contain the expected value %d\n", THREAD_COUNT);
  } else {
    printf("Error: array contains %lld at index %d, but value %lld was expected\n", A[invalid_index], invalid_index, THREAD_COUNT);
    return 1;
  }

  return 0;
}
