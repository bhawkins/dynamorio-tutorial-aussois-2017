#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <fstream>
#include <string.h>

#define ARRAY_SIZE 1024
#define ARRAY_ALLOC_SIZE (ARRAY_SIZE * sizeof(uint64_t))
#define THREAD_COUNT 3

#define ENV_INPUT "PD_INPUT"

static uint64_t *A, *I[THREAD_COUNT];

static std::mutex m;

static const char *input_dir = NULL;

static void load_input()
{
  char filename[1024] = {0};

  for (int i = 0; i < THREAD_COUNT; i++) {
    I[i] = (uint64_t *) malloc(ARRAY_ALLOC_SIZE);
    memset(I[i], 0, ARRAY_ALLOC_SIZE);

    int j = 0;
    uint64_t a;
    snprintf(filename, 1024, "%s/input%d.dat", input_dir, i);
    std::ifstream input(filename);
    while (input >> a)
      I[i][j++] = a;
  }
}

static void
write_thread(int input_set)
{
  int index;

  for (int i = 0; i < ARRAY_SIZE; i++) {
    index = I[input_set][i];
    A[index]++;
  }
}

static void
write()
{
}

static int
check_array(uint64_t expected_value)
{
  for (int i = 0; i < ARRAY_SIZE; i++) {
    if (A[i] != expected_value)
      return (i+1);
  }
  return 0;
}

static void
print_input()
{
  for (int i = 0; i < THREAD_COUNT; i++) {
    for (int j = 0; j < ARRAY_SIZE; j++)
      printf("I[%d][%d]: %d\n", i, j, I[i][j]);
  }
}

int
main(int argc, const char *argv[])
{
  input_dir = getenv(ENV_INPUT);
  if (input_dir == NULL) {
    fprintf(stderr, "Environment variable " ENV_INPUT " must specify a valid path.\n");
    exit(1);
  }

  A = (uint64_t *) malloc(ARRAY_ALLOC_SIZE);
  memset(A, 0, ARRAY_ALLOC_SIZE);
  load_input();

  std::thread *threads[THREAD_COUNT];

  auto start = std::chrono::steady_clock::now();

  threads[0] = new std::thread(thread0);
  threads[1] = new std::thread(thread1);
  threads[2] = new std::thread(thread2);

  for (int i = 0; i < THREAD_COUNT; i++)
    threads[i]->join();

  auto duration = (std::chrono::steady_clock::now() - start);

  printf("Duration is %lld\n", duration);

  print_input();
  /*
  int invalid_index = check_array(THREAD_COUNT);
  if (invalid_index == 0) {
    printf("Success! All array entries contain the expected value %d\n", THREAD_COUNT);
  } else {
    printf("Error: array contains %lld at index %d, but value %lld was expected\n", A[invalid_index], invalid_index, THREAD_COUNT);
    return 1;
  }
  */

  return 0;
}
