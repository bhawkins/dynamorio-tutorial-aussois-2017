#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <fstream>
#include <string.h>

#define ARRAY_SIZE 1024
#define ARRAY_ALLOC_SIZE (ARRAY_SIZE * sizeof(uint64_t))
#define THREAD_COUNT 3

#define ENV_INPUT "PD_INPUT"

static uint64_t *A; /* target of increment operations */
static uint64_t *I[THREAD_COUNT]; /* map of increment index targets, per thread */
static uint64_t *E; /* expected values */

#define LOCK_COUNT 12
static std::mutex locks[LOCK_COUNT];

#define OUTER_LOOP_ITERATIONS (ARRAY_SIZE >> 8)
#define INNER_LOOP_ITERATIONS 256

static const char *input_dir = NULL;

static uint64_t *
load_file(const char *filename)
{
  uint64_t *v = (uint64_t *) malloc(ARRAY_ALLOC_SIZE);
  memset(v, 0, ARRAY_ALLOC_SIZE);

  int j = 0;
  uint64_t a;
  std::ifstream input(filename);
  while (input >> a)
    v[j++] = a;

  return v;
}

static void load_input()
{
  char filename[1024] = {0};

  for (int i = 0; i < THREAD_COUNT; i++) {
    snprintf(filename, 1024, "%s/input%d.dat", input_dir, i);
    I[i] = load_file(filename);
  }
  snprintf(filename, 1024, "%s/expected.dat", input_dir);
  E = load_file(filename);
}

static inline void
kernel_thread_inner(int input_set, int start)
{
  int index;

  for (int i = start; i < (start + INNER_LOOP_ITERATIONS); i++) {
    index = I[input_set][i];
    A[index]++;
  }
}

static void
kernel_thread(int input_set, int iterations)
{
  for (int i = 0; i < iterations; i++) {
    switch (i) {
      case 0:
        kernel_thread_inner(input_set, 0);
        break;
      case 1:
        kernel_thread_inner(input_set, INNER_LOOP_ITERATIONS);
        break;
      case 2:
        kernel_thread_inner(input_set, 2 * INNER_LOOP_ITERATIONS);
        break;
      case 3:
        kernel_thread_inner(input_set, 3 * INNER_LOOP_ITERATIONS);
        break;
      default:
        kernel_thread_inner(input_set, 4 * INNER_LOOP_ITERATIONS);
        return;
    }
  }
}

static void __attribute__ ((noinline))
kernel()
{
  memset(A, 0, ARRAY_ALLOC_SIZE);

  std::thread *threads[THREAD_COUNT];

  for (int i = 0; i < THREAD_COUNT; i++)
    threads[i] = new std::thread(kernel_thread, i, OUTER_LOOP_ITERATIONS);

  for (int i = 0; i < THREAD_COUNT; i++)
    threads[i]->join();
}

static int
check_array(uint64_t expected_value)
{
  for (int i = 0; i < ARRAY_SIZE; i++) {
    if (A[i] != E[i])
      return i;
  }
  return -1;
}

static void
print_input()
{
  for (int i = 0; i < THREAD_COUNT; i++) {
    for (int j = 0; j < ARRAY_SIZE; j++)
      printf("I[%d][%d]: %d\n", i, j, I[i][j]);
  }
}

int
main(int argc, const char *argv[])
{
  for (int i = 0; i < LOCK_COUNT; i++) {
    locks[i].lock();
    locks[i].unlock();
  }
  // m.lock(); /* coerce g++ into putting lock/unlock in the PLT */
  // m.unlock();

  input_dir = getenv(ENV_INPUT);
  if (input_dir == NULL) {
    fprintf(stderr, "Environment variable " ENV_INPUT " must specify a valid path.\n");
    exit(1);
  }

  A = (uint64_t *) malloc(ARRAY_ALLOC_SIZE);
  load_input();
  kernel();

  // print_input();
  int invalid_index = check_array(THREAD_COUNT);
  if (invalid_index < 0) {
    printf("Success! All array entries contain the expected value\n");
  } else {
    printf("Error: array contains %lld at index %d, but value %lld was expected\n",
           A[invalid_index], invalid_index, E[invalid_index]);
    return 1;
  }

  return 0;
}
