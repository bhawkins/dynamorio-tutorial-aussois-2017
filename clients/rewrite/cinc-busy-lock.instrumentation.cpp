
#include <map>

#include "instrumentation.hpp"
#include "drwrap.h"

#define KERNEL_START 0xfb8
#define KERNEL_END   0x100a
#define SEMAPHORE    0x604440

static std::map<std::string, uint64_t> InstrMap = {{ "increment",     0x100a },
                                                   { "Lock after",    0xfbc  },
                                                   { "Lock start",    0xfc9  },
                                                   { "Unlock start",  0xff1  },
                                                   { "Unlock after",  0x1008 },
                                                   { "Atomicize",     0xfe7  },
                                                   { "plt:lock()",    0x1324 },
                                                   { "plt:unlock()",  0x1352 }};

#define LOCK_SPAN 0xa

void
pre_increment(void *wrapcxt, OUT void **user_data)
{
    timer::time_point *start = new timer::time_point(timer::now());
    *user_data = (void *) start;

    dr_printf("Start: %lld\n", start->time_since_epoch().count());
}

void
post_increment(void *wrapcxt, void *user_data)
{
    timer::time_point *start = (timer::time_point *) user_data;
    dr_printf("total runtime: %lld\n", (timer::now() - *start).count());

    delete start;
}

static inline bool
instr_in_range(uint64_t src_offset, uint64_t dst_offset, uint64_t span) {
    return (src_offset >= dst_offset && src_offset < (dst_offset + span));
}

/* ========== CIncBusyLock ========== */

void
CIncBusyLock::init()
{
    drwrap_wrap((app_pc) (main_module_base + InstrMap.at("increment")),
                pre_increment, post_increment);

    dr_printf("Init cinc-busy-lock instrumentation\n");
}

dr_emit_flags_t
CIncBusyLock::event_app2app(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                            bool translating)
{
    /* Pre-built instrumentation */

    print_kernel_tags(drcontext, tag, bb, for_trace, translating);

    if (false) { /* disabled calls (avoids "unused" compiler warnings */
        print_instr_map(drcontext, tag, bb, for_trace, translating);
        remove_inner_locks(drcontext, tag, bb, for_trace, translating);
        add_outer_locks(drcontext, tag, bb, for_trace, translating);
        atomicize_inc(drcontext, tag, bb, for_trace, translating);
        print_bb(drcontext, tag, bb, for_trace, translating);
    }

    return DR_EMIT_DEFAULT;
}

/* Clobbers %rax. Sets flags for jnz on fail. */
void
CIncBusyLock::insert_atomic_inc(void *drcontext, instrlist_t *bb, instr_t *before_me,
                                reg_id_t dst_reg, reg_id_t value_reg)
{
    opnd_t dst = opnd_create_reg(dst_reg);
    opnd_t value = opnd_create_reg(value_reg);
    opnd_t old = opnd_create_reg(DR_REG_RAX);

    app_pc pc = instr_get_app_pc(before_me);

    /* mov $value,%rax */
    instr_t *set_old = XINST_CREATE_move(drcontext, old, value);
    instr_set_translation(set_old, pc);
    instrlist_preinsert(bb, before_me, set_old);

    /* $value++ */
    instr_t *inc = XINST_CREATE_add(drcontext, value, OPND_CREATE_INT32(1));
    instr_set_translation(inc, pc);
    instrlist_preinsert(bb, before_me, set_old);

    /* cmpxchg ($dst), $value */
    instr_t *swap = INSTR_CREATE_cmpxchg_8(drcontext, dst, value);
    instr_set_translation(swap, pc);
    instrlist_preinsert(bb, before_me, swap);
}

/* Clobbers %rax. Sets flags for jnz on fail. */
void
CIncBusyLock::swapify_inc(void *drcontext, instrlist_t *bb, instr_t *inc, opnd_t dst_reg)
{
    opnd_t value = instr_get_dst(inc, 0);
    opnd_t old = opnd_create_reg(DR_REG_RAX);
    opnd_t dst = OPND_CREATE_MEM64(opnd_get_reg(dst_reg), 0);

    app_pc inc_pc = instr_get_app_pc(inc);

    /* mov $value,%rax */
    instr_t *set_old = XINST_CREATE_move(drcontext, old, value);
    instr_set_translation(set_old, inc_pc);
    instrlist_preinsert(bb, inc, set_old);

    /* cmpxchg ($dst), $value */
    instr_t *swap = INSTR_CREATE_cmpxchg_8(drcontext, dst, value);
    LOCK(swap);
    instr_set_translation(swap, inc_pc);
    instrlist_postinsert(bb, inc, swap);
}

void
CIncBusyLock::insert_lock_call(void *drcontext, instrlist_t *bb, instr_t *return_to,
                               uint64_t plt_offset)
{
    instr_t *instr;
    app_pc target = (app_pc) main_module_base + plt_offset;
    app_pc return_address = instr_get_app_pc(return_to);

    /*  mov $SEMAPHORE,%edi */
    instr_t *arg = INSTR_CREATE_mov_imm(drcontext, opnd_create_reg(DR_REG_EDI),
                                        OPND_CREATE_INT32(SEMAPHORE));
    instr_set_translation(arg, return_address - 1);
    instrlist_preinsert(bb, return_to, arg);

    /* pushq &return_to */
    instr_t *lock_push = INSTR_CREATE_push_imm(drcontext,
                                               OPND_CREATE_INT32(return_address));
    instr_set_translation(lock_push, return_address - 1);
    instrlist_preinsert(bb, return_to, lock_push);

    /* jumpq target */
    instr_t *lock_jump = XINST_CREATE_jump(drcontext, opnd_create_pc(target));
    instr_set_translation(lock_jump, return_address - 1);
    instrlist_preinsert(bb, return_to, lock_jump);

    /* remove remaining instructions (will be translated where our new call returns */
    instr = return_to;
    do {
        instr_t *removal = instr;
        instr = instr_get_next(instr);
        instrlist_remove(bb, removal);
        instr_destroy(drcontext, removal);
    } while (instr != NULL);
}

void
CIncBusyLock::remove_inner_locks(void *drcontext, void *tag, instrlist_t *bb,
                                 bool for_trace, bool translating)
{
    instr_t *instr = instrlist_first(bb);

    while (instr != NULL) {
        app_pc instr_pc = instr_get_app_pc(instr);
        uint64_t instr_offset = (uint64_t) instr_pc - main_module_base;

        if (instr_in_range(instr_offset, InstrMap.at("Lock start"), LOCK_SPAN) ||
            instr_in_range(instr_offset, InstrMap.at("Unlock start"), LOCK_SPAN)) {
            instr_t *removal = instr;

            // dr_printf("Removing instr at 0x%llx", instr_pc);
            // disassemble(drcontext, instr_pc, STDOUT);

            instr = instr_get_next(instr);
            instrlist_remove(bb, removal);
            instr_destroy(drcontext, removal);
        } else {
            instr = instr_get_next(instr);
        }
    }
}

void
CIncBusyLock::add_outer_locks(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                              bool translating)
{
    instr_t *instr = instrlist_first(bb);

    while (instr != NULL) {
        app_pc instr_pc = instr_get_app_pc(instr);
        uint64_t instr_offset = (uint64_t) instr_pc - main_module_base;

        if (instr_offset == InstrMap.at("Lock after")) {
            insert_lock_call(drcontext, bb, instr_get_next(instr), InstrMap.at("plt:lock()"));
            break;
        } else if (instr_offset == InstrMap.at("Unlock after")) {
            insert_lock_call(drcontext, bb, instr_get_next(instr), InstrMap.at("plt:unlock()"));
            break;
        } else {
            instr = instr_get_next(instr);
        }
    }
}

void
CIncBusyLock::atomicize_inc(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                            bool translating)
{
    instr_t *instr = instrlist_first(bb);

    while (instr != NULL) {
        app_pc instr_pc = instr_get_app_pc(instr);
        uint64_t instr_offset = (uint64_t) instr_pc - main_module_base;

        if (instr_offset == InstrMap.at("Atomicize")) {
            instr_t *load = instr;
            instr_t *inc = instr_get_next(load);
            instr_t *store = instr_get_next(inc);

            opnd_t addr = opnd_create_reg(DR_REG_RAX);
            opnd_t dst = opnd_create_reg(DR_REG_RDI);

            /* mov %rax %rdi */
            instr_t *set_dst = XINST_CREATE_move(drcontext, dst, addr);
            instr_set_translation(set_dst, instr_get_app_pc(load));
            instrlist_preinsert(bb, inc, set_dst);

            swapify_inc(drcontext, bb, inc, dst);

            /* mov %rdi %rax */
            instr_t *restore_addr = XINST_CREATE_move(drcontext, addr, dst);
            instr_set_translation(restore_addr, instr_get_app_pc(store));
            instrlist_preinsert(bb, store, restore_addr);

            /* jnz @load */
            instr_t *retry = INSTR_CREATE_jcc(drcontext, OP_jnz, opnd_create_instr(load));
            instr_set_translation(retry, instr_get_app_pc(store));
            instrlist_preinsert(bb, store, retry);

            instrlist_remove(bb, store);
            instr_destroy(drcontext, store);
            return;
        } else {
            instr = instr_get_next(instr);
        }
    }
}

void
CIncBusyLock::print_kernel_tags(void *drcontext, void *tag, instrlist_t *bb,
                                bool for_trace, bool translating)
{
    uint64_t bb_offset = (uint64_t) tag - main_module_base;

    if (!for_trace &&
        bb_offset >= KERNEL_START &&
        bb_offset <= KERNEL_END)
        dr_printf("Found kernel bb 0x%llx [%stranslating|%sfor_trace]\n",
                  (uint64_t) tag, translating ? "+" : "-", for_trace ? "+" : "-");
}

void
CIncBusyLock::print_instr_map(void *drcontext, void *tag, instrlist_t *bb,
                              bool for_trace, bool translating)
{
    instr_t *instr = instrlist_first(bb);

    while (instr != NULL) {
        app_pc instr_pc = instr_get_app_pc(instr);
        uint64_t instr_offset = (uint64_t) instr_pc - main_module_base;

        if (!for_trace) {
            std::map<std::string, uint64_t>::iterator i = InstrMap.begin();
            for (; i != InstrMap.end(); ++i) {
                if (instr_offset == i->second) {
                    dr_printf("%s ", i->first.c_str());
                    disassemble(drcontext, instr_pc, STDOUT);
                }
            }
        }
        instr = instr_get_next(instr);
    }
}

void
CIncBusyLock::print_bb(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                       bool translating)
{
    uint64_t bb_offset = (uint64_t) tag - main_module_base;

    if (!for_trace &&
        bb_offset >= KERNEL_START &&
        bb_offset < KERNEL_END)
        instrlist_disassemble(drcontext, (app_pc) tag, bb, STDOUT);
}
