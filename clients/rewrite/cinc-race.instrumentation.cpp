
#include <map>

#include "instrumentation.hpp"
#include "drwrap.h"

/*
 * Fill in these values from an assembly dump of cinc-race (objdump -d)
 */

#define KERNEL_START 0
#define KERNEL_END   0
#define SEMAPHORE    0

static std::map<std::string, uint64_t> InstrMap = {{ "increment",     0 },
                                                   { "Lock after",    0 },
                                                   { "Lock start",    0 },
                                                   { "Unlock start",  0 },
                                                   { "Unlock after",  0 },
                                                   { "plt:lock()",    0 },
                                                   { "plt:unlock()",  0 }};

#define LOCK_SPAN 0xa

static inline bool
instr_in_range(uint64_t src_offset, uint64_t dst_offset, uint64_t span) {
    return (src_offset >= dst_offset && src_offset < (dst_offset + span));
}

/* ========== CIncRace ========== */

void
CIncRace::init()
{
    dr_printf("Init cinc-race instrumentation\n");
}

dr_emit_flags_t
CIncRace::event_app2app(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                            bool translating)
{
    return DR_EMIT_DEFAULT;
}

void
CIncRace::insert_lock_call(void *drcontext, instrlist_t *bb, instr_t *return_to,
                           uint64_t plt_offset)
{
    instr_t *instr;
    app_pc target = (app_pc) main_module_base + plt_offset;
    app_pc return_address = instr_get_app_pc(return_to);

    /*  mov $SEMAPHORE,%edi */
    instr_t *arg = INSTR_CREATE_mov_imm(drcontext, opnd_create_reg(DR_REG_EDI),
                                        OPND_CREATE_INT32(SEMAPHORE));
    instr_set_translation(arg, return_address - 1);
    instrlist_preinsert(bb, return_to, arg);

    /* pushq &return_to */
    instr_t *lock_push = INSTR_CREATE_push_imm(drcontext,
                                               OPND_CREATE_INT32(return_address));
    instr_set_translation(lock_push, return_address - 1);
    instrlist_preinsert(bb, return_to, lock_push);

    /* jumpq target */
    instr_t *lock_jump = XINST_CREATE_jump(drcontext, opnd_create_pc(target));
    instr_set_translation(lock_jump, return_address - 1);
    instrlist_preinsert(bb, return_to, lock_jump);

    /* remove remaining instructions (will be translated where our new call returns */
    instr = return_to;
    do {
        instr_t *removal = instr;
        instr = instr_get_next(instr);
        instrlist_remove(bb, removal);
        instr_destroy(drcontext, removal);
    } while (instr != NULL);
}

