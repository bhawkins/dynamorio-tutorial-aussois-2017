
#pragma once

#include <string>
#include <chrono>

#include "dr_api.h"

typedef std::chrono::steady_clock timer;

class RewriteInstrumentation
{
public:

    static uint64_t main_module_base;

    virtual void
    init();

    virtual dr_emit_flags_t
    event_app2app(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                  bool translating) = 0;
};

class CIncBusyLock : public RewriteInstrumentation
{
public:

    void
    init();

    dr_emit_flags_t
    event_app2app(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                  bool translating);

    /* helper methods */

    void
    insert_atomic_inc(void *drcontext, instrlist_t *bb, instr_t *before_me,
                      reg_id_t dst, reg_id_t value);

    void
    swapify_inc(void *drcontext, instrlist_t *bb, instr_t *inc, opnd_t dst_reg);

    void
    insert_lock_call(void *drcontext, instrlist_t *bb, instr_t *return_to,
                     uint64_t plt_offset);

    /* top-level methods */

    void
    remove_inner_locks(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                       bool translating);

    void
    add_outer_locks(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                    bool translating);

    void
    atomicize_inc(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                  bool translating);

    void
    print_kernel_tags(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                      bool translating);

    void
    print_instr_map(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                    bool translating);

    void
    print_bb(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
             bool translating);

};

class CIncRace : public RewriteInstrumentation
{
public:

    void
    init();

    dr_emit_flags_t
    event_app2app(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                  bool translating);

    /* helper methods */

    void
    insert_lock_call(void *drcontext, instrlist_t *bb, instr_t *return_to,
                     uint64_t plt_offset);

};

class Instrumentation {

    static RewriteInstrumentation *instrumentation;

public:

    static void
    init(std::string *app_name)
    {
        if (app_name->compare("cinc-busy-lock") == 0)
            instrumentation = new CIncBusyLock();
        else if (app_name->compare("cinc-race") == 0)
            instrumentation = new CIncRace();
        else
            dr_fprintf(STDERR, "Error: no instrumentation for app '%s'\n", app_name->c_str());

        DR_ASSERT(instrumentation != NULL);

        instrumentation->init();
    }

    static dr_emit_flags_t
    event_app2app(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                  bool translating)
    {
        return instrumentation->event_app2app(drcontext, tag, bb, for_trace, translating);
    }
};
