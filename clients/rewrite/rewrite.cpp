/* ******************************************************************************
 * Copyright (c) 2015 Google, Inc.  All rights reserved.
 * ******************************************************************************/

/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of Google, Inc. nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL VMWARE, INC. OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

/* goulots.cpp: client for analyzing bottlenecks in compute benchmarks */

#include <string>
#include <cstdint>
#include <stdio.h>
#include <string.h> /* for memset */
#include <stddef.h> /* for offsetof */
#include "dr_api.h"
#include "drmgr.h"
#include "drwrap.h"
#include "drsyms.h"
#include "drutil.h"

#include "instrumentation.hpp"

// XXX i#1732: make a msgbox on Windows (controlled by option for batch runs)
#define NOTIFY(level, ...) do {            \
    if (op_verbose.get_value() >= (level)) \
        dr_fprintf(STDERR, __VA_ARGS__);   \
} while (0)

#ifdef WINDOWS
# define IF_WINDOWS(x) x
#else
# define IF_WINDOWS(x) /* nothing */
#endif

static client_id_t client_id;
uint64_t RewriteInstrumentation::main_module_base = 0;
RewriteInstrumentation *Instrumentation::instrumentation = NULL;

static void
event_thread_init(void *drcontext)
{
}

static void
event_thread_exit(void *drcontext)
{
}

static void
event_exit(void)
{
    if (!drmgr_unregister_thread_init_event(event_thread_init) ||
        !drmgr_unregister_thread_exit_event(event_thread_exit))
        DR_ASSERT(false);

    drutil_exit();
    drmgr_exit();
    drwrap_exit();
    drsym_exit();
}

DR_EXPORT void
dr_client_main(client_id_t id, int argc, const char *argv[])
{
    std::string *app_name;

    dr_set_client_name("App Rewriter", "");

    if (!drmgr_init())
        DR_ASSERT(false);
    if (!drwrap_init())
        DR_ASSERT(false);
    if (drsym_init(0) != DRSYM_SUCCESS)
        DR_ASSERT(false);
    if (!drutil_init())
        DR_ASSERT(false);

    client_id = id;

    module_data_t *main_module = dr_get_main_module();
    RewriteInstrumentation::main_module_base = (uint64_t) main_module->start;
    app_name = new std::string(strdup(dr_module_preferred_name(main_module)));
    dr_free_module_data(main_module);

    Instrumentation::init(app_name);

    /* register events */
    dr_register_exit_event(event_exit);
    if (!drmgr_register_bb_app2app_event(Instrumentation::event_app2app, NULL) ||
        !drmgr_register_thread_init_event(event_thread_init) ||
        !drmgr_register_thread_exit_event(event_thread_exit)) {
        /* something is wrong: can't continue */
        DR_ASSERT(false);
        return;
    }

    /* make it easy to tell, by looking at log file, which client executed */
    dr_log(NULL, LOG_ALL, 1, "Initializing client 'App Rewriter' for app '%s'\n",
           app_name->c_str());
    if (dr_is_notify_on()) {
# ifdef WINDOWS
        /* ask for best-effort printing to cmd window.  must be called at init. */
        dr_enable_console_printing();
# endif
        dr_printf("App Rewriter finds main module '%s' at 0x%llx\n",
                  app_name->c_str(), RewriteInstrumentation::main_module_base);
    }

    delete(app_name);
}
