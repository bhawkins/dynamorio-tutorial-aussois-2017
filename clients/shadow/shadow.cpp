/* ******************************************************************************
 * Copyright (c) 2015 Google, Inc.  All rights reserved.
 * ******************************************************************************/

/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of Google, Inc. nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL VMWARE, INC. OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

/* shadow.cpp: client for the "partially disjoint" app */

#include <stdio.h>
#include <string.h> /* for memset */
#include <stddef.h> /* for offsetof */
#include <vector>
#include <string>
#include <map>
#include <sstream>
#include "dr_api.h"
#include "drmgr.h"
#include "drwrap.h"
#include "drsyms.h"
#include "drreg.h"
#include "drutil.h"
#include "utils.h"

#define ENV_OUTPUT "SHADOW_OUTPUT"

static std::map<std::string, uint64_t> IncMap = {{ "inc0",     0x160d },
                                                 { "inc1",     0x1623 },
                                                 { "inc2",     0x1639 },
                                                 { "inc3",     0x164c }};

#define TOP_LOOP_ENTRY 1ULL

static uint64_t loop_heads[] = { 0x15f9, 0x16fb, 0x16d1, 0x16aa };
static uint64_t loop_tails[] = { 0x166f, 0x169a, 0x16c1, 0x16eb };

#define PLT_MUTEX_LOCK 0x1480
#define PLT_MUTEX_UNLOCK 0x14d0
#define LOCK_COUNT 12
#define LOCK_BASE 0x6036c0ULL
#define LOCK_SIZE 0x28ULL

static app_pc locks[LOCK_COUNT];

#define STORE_COUNT 4

typedef struct _code_region_t {
    uint64_t start;
    uint64_t end;
} code_region_t;

static uint64_t main_module_base = 0;
static app_pc app_kernel_function = NULL;

// XXX i#1732: make a msgbox on Windows (controlled by option for batch runs)
#define NOTIFY(level, ...) do {            \
    if (op_verbose.get_value() >= (level)) \
        dr_fprintf(STDERR, __VA_ARGS__);   \
} while (0)

#ifdef WINDOWS
# define IF_WINDOWS(x) x
#else
# define IF_WINDOWS(x) /* nothing */
#endif

/* Each mem_ref_t includes the type of reference (read or write),
 * the address referenced, and the size of the reference.
 */
typedef struct _mem_ref_t {
    bool  is_write;
    void *addr;
    size_t size;
    app_pc pc;
} mem_ref_t;

/* Max number of mem_ref a buffer can have */
#define MAX_NUM_MEM_REFS 8192
/* The size of memory buffer for holding mem_refs. When it fills up,
 * we dump data from the buffer to the file.
 */
#define MEM_BUF_SIZE (sizeof(mem_ref_t) * MAX_NUM_MEM_REFS)

/* thread private log file and counter */
typedef struct _per_thread_t {
    int       thread_index;
    char      *mem_buf_ptr;
    char      *mem_buf_base;
    /* buf_end holds the negative value of real address of buffer end. */
    ptr_int_t buf_end;
    void      *cache;
    file_t    trace_file;
    uint64    num_refs;
} per_thread_t;

static size_t page_size;
static client_id_t client_id;
static app_pc code_cache;
static void  *mutex;    /* for multithread support */
static uint64 num_refs; /* keep a global memory reference count */
static int tls_index;

typedef struct _thread_stores_t {
    std::vector<app_pc> store_targets[STORE_COUNT];
} thread_stores_t;

#define KERNEL_START 0x15e6ULL
#define KERNEL_END 0x1717ULL
#define KERNEL_OFFSET 0x1a19
#define KERNEL_THREAD_COUNT 3
static bool is_main_thread = true;
static int thread_index = 0;
static thread_stores_t threads[KERNEL_THREAD_COUNT];

static bool shadow_phase = true;

static void instrument_mem(void        *drcontext,
                           instrlist_t *ilist,
                           instr_t     *where,
                           int          pos,
                           bool         is_write);

static int
lookup_store_index(app_pc store_pc)
{
    std::map<std::string, uint64_t>::iterator i = IncMap.begin();
    for (; i != IncMap.end(); ++i) {
        if ((app_pc) (main_module_base + i->second) == store_pc)
            return std::distance(IncMap.begin(), i);
    }
    return -1;
}

static void
memtrace(void *drcontext)
{
    per_thread_t *data;
    int i, num_refs, store_index;
    mem_ref_t *mem_ref;

    data      = (per_thread_t *) drmgr_get_tls_field(drcontext, tls_index);
    mem_ref   = (mem_ref_t *)data->mem_buf_base;
    num_refs  = (int)((mem_ref_t *)data->mem_buf_ptr - mem_ref);

    for (i = 0; i < num_refs; i++) {
        dr_fprintf(data->trace_file, PFX " " PFX " %d %d\n", mem_ref->pc,
                   mem_ref->addr, mem_ref->size, mem_ref->is_write);

        if (mem_ref->is_write) {
            store_index = lookup_store_index(mem_ref->pc);
            if (store_index >= 0)
                threads[data->thread_index].store_targets[store_index].push_back((app_pc) mem_ref->addr);
        }

        ++mem_ref;
    }
    data->num_refs += num_refs;

    memset(data->mem_buf_base, 0, MEM_BUF_SIZE);
    data->mem_buf_ptr = data->mem_buf_base;
}

/* we transform string loops into regular loops so we can more easily
 * monitor every memory reference they make
 */
static dr_emit_flags_t
event_bb_app2app(void *drcontext, void *tag, instrlist_t *bb,
                 bool for_trace, bool translating)
{
    if (!drutil_expand_rep_string(drcontext, bb)) {
        DR_ASSERT(false);
        /* in release build, carry on: we'll just miss per-iter refs */
    }
    return DR_EMIT_DEFAULT;
}

static void
insert_lock_call(void *drcontext, instrlist_t *bb, instr_t *return_to, int lock_index,
                 uint64_t plt_offset)
{
    app_pc target = (app_pc) main_module_base + plt_offset;

    dr_prepare_for_call(drcontext, bb, return_to);
    dr_insert_call(drcontext, bb, return_to, target, 1,
                   OPND_CREATE_INT32(locks[lock_index]));
    dr_cleanup_after_call(drcontext, bb, return_to, 0);
}

static bool
is_loop_head(uint64_t pc)
{
    for (int i = 0; i < STORE_COUNT; i++) {
        if (loop_heads[i] == pc)
            return true;
    }
    return false;
}

static bool
is_loop_tail(uint64_t pc)
{
    for (int i = 0; i < STORE_COUNT; i++) {
        if (loop_tails[i] == pc)
            return true;
    }
    return false;
}

/* event_bb_insert calls instrument_mem to instrument every
 * application memory reference.
 */
static dr_emit_flags_t
event_bb_insert(void *drcontext, void *tag, instrlist_t *bb,
                instr_t *instr, bool for_trace, bool translating,
                void *user_data)
{
    int i;
    app_pc instr_pc = instr_get_app_pc(instr);

    if (shadow_phase) {
        if (lookup_store_index(instr_pc) < 0)
            return DR_EMIT_DEFAULT;

        if (instr_reads_memory(instr)) {
            for (i = 0; i < instr_num_srcs(instr); i++) {
                if (opnd_is_memory_reference(instr_get_src(instr, i))) {
                    instrument_mem(drcontext, bb, instr, i, false);
                }
            }
        }
        if (instr_writes_memory(instr)) {
            for (i = 0; i < instr_num_dsts(instr); i++) {
                if (opnd_is_memory_reference(instr_get_dst(instr, i))) {
                    instrument_mem(drcontext, bb, instr, i, true);
                }
            }
        }
    }
    return DR_EMIT_DEFAULT;
}

static void __attribute__ ((unused))
print_bb(void *drcontext, void *tag, instrlist_t *bb, bool for_trace, bool translating)
{
    uint64_t bb_offset = (uint64_t) tag - main_module_base;

    if (!for_trace && bb_offset >= KERNEL_START && bb_offset < KERNEL_END)
        instrlist_disassemble(drcontext, (app_pc) tag, bb, STDOUT);
}

static dr_emit_flags_t
event_app2app(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
              bool translating)
{
    instr_t *instr = instrlist_first(bb);

    if (!shadow_phase) {
        instr = instrlist_first(bb);

        while (instr != NULL) {
            app_pc instr_pc = instr_get_app_pc(instr);
            uint64_t instr_offset = (uint64_t) instr_pc - main_module_base;

            if (is_loop_head(instr_offset)) {
                insert_lock_call(drcontext, bb, instr_get_next(instr), 0, PLT_MUTEX_LOCK);
                break;
            }

            if (is_loop_tail(instr_offset)) {
                insert_lock_call(drcontext, bb, instr_get_next(instr), 0, PLT_MUTEX_UNLOCK);
                break;
            }

            instr = instr_get_next(instr);
        }
    }
    return DR_EMIT_DEFAULT;
}

/* write_and_flush_buffer dumps the memory reference info to the log file */
static void
write_and_flush_buffer(void)
{
    void *drcontext = dr_get_current_drcontext();
    memtrace(drcontext);
}

static void
code_cache_init(void)
{
    void         *drcontext;
    instrlist_t  *ilist;
    instr_t      *where;
    byte         *end;

    drcontext  = dr_get_current_drcontext();
    code_cache = (app_pc) dr_nonheap_alloc(page_size,
                                           DR_MEMPROT_READ  |
                                           DR_MEMPROT_WRITE |
                                           DR_MEMPROT_EXEC);
    ilist = instrlist_create(drcontext);
    /* The lean procecure simply performs a clean call, and then jump back */
    /* jump back to the DR's code cache */
    where = INSTR_CREATE_jmp_ind(drcontext, opnd_create_reg(DR_REG_XCX));
    instrlist_meta_append(ilist, where);
    /* clean call */
    dr_insert_clean_call(drcontext, ilist, where, (void *) write_and_flush_buffer, false, 0);
    /* Encodes the instructions into memory and then cleans up. */
    end = instrlist_encode(drcontext, ilist, code_cache, false);
    DR_ASSERT((size_t)(end - code_cache) < page_size);
    instrlist_clear_and_destroy(drcontext, ilist);
    /* set the memory as just +rx now */
    dr_memory_protect(code_cache, page_size, DR_MEMPROT_READ | DR_MEMPROT_EXEC);
}

static void
code_cache_exit(void)
{
    dr_nonheap_free(code_cache, page_size);
}

static void
event_thread_init(void *drcontext)
{
#define MAX_PATH_CHARS 512

    per_thread_t *data;
    const char *trace_dir = "./pd-traces", *env_trace_dir;
    char trace_file_path[MAX_PATH_CHARS];

    /* allocate thread private data */
    data = (per_thread_t *) dr_thread_alloc(drcontext, sizeof(per_thread_t));
    drmgr_set_tls_field(drcontext, tls_index, data);
    data->mem_buf_base = (char *) dr_thread_alloc(drcontext, MEM_BUF_SIZE);
    data->mem_buf_ptr  = data->mem_buf_base;
    /* set buf_end to be negative of address of buffer end for the lea later */
    data->buf_end  = -(ptr_int_t)(data->mem_buf_base + MEM_BUF_SIZE);
    data->num_refs = 0;

    /* We're going to dump our data to a per-thread file.
     * On Windows we need an absolute path so we place it in
     * the same directory as our library. We could also pass
     * in a path as a client argument.
     */
    env_trace_dir = getenv(ENV_OUTPUT);
    if (env_trace_dir != NULL)
        trace_dir = env_trace_dir;
    dr_snprintf(trace_file_path, MAX_PATH_CHARS, "%s/%s.%lx.traces.log", trace_dir,
                dr_get_application_name(), dr_get_thread_id(drcontext));
    data->trace_file = dr_open_file(trace_file_path, DR_FILE_WRITE_REQUIRE_NEW);

    if (is_main_thread) {
        is_main_thread = false;
    } else {
        dr_mutex_lock(mutex);
        data->thread_index = thread_index++;
        dr_mutex_unlock(mutex);
    }
}

static void
event_thread_exit(void *drcontext)
{
    per_thread_t *data;

    memtrace(drcontext);
    data = (per_thread_t *) drmgr_get_tls_field(drcontext, tls_index);
    dr_mutex_lock(mutex);
    num_refs += data->num_refs;
    dr_mutex_unlock(mutex);
    dr_flush_file(data->trace_file);
    log_file_close(data->trace_file);
    dr_thread_free(drcontext, data->mem_buf_base, MEM_BUF_SIZE);
    dr_thread_free(drcontext, data, sizeof(per_thread_t));
}

static void
event_exit(void)
{
#ifdef SHOW_RESULTS
    char msg[512];
    int len;
    len = dr_snprintf(msg, sizeof(msg)/sizeof(msg[0]),
                      "Instrumentation results:\n"
                      "  saw %llu memory references\n",
                      num_refs);
    DR_ASSERT(len > 0);
    NULL_TERMINATE_BUFFER(msg);
    DISPLAY_STRING(msg);
#endif /* SHOW_RESULTS */
    code_cache_exit();

    if (!drmgr_unregister_tls_field(tls_index) ||
        !drmgr_unregister_thread_init_event(event_thread_init) ||
        !drmgr_unregister_thread_exit_event(event_thread_exit) ||
        !drmgr_unregister_bb_insertion_event(event_bb_insert) ||
        drreg_exit() != DRREG_SUCCESS)
        DR_ASSERT(false);

    dr_mutex_destroy(mutex);
    drutil_exit();
    drmgr_exit();
    drwrap_exit();
    drsym_exit();
}

static void
reset_threads()
{
    for (int i = 0; i < thread_index; i++) {
        for (int j = 0; j < STORE_COUNT; j++)
            threads[i].store_targets[j].clear();
    }
    thread_index = 0;
}

void
pre_write(void *wrapcxt, OUT void **user_data)
{
    dr_mcontext_t *mcontext = drwrap_get_mcontext(wrapcxt);
    *user_data = (void *) mcontext->rsp;
}

void
post_write(void *wrapcxt, void *user_data)
{
    if (shadow_phase) {
        dr_mcontext_t *mcontext = drwrap_get_mcontext(wrapcxt);
        mcontext->rip = app_kernel_function;
        mcontext->rsp = (reg_t) user_data;

        shadow_phase = false;

        dr_delay_flush_region((app_pc) (main_module_base + IncMap.at("inc0")),
                              IncMap.at("inc3") - IncMap.at("inc0"), 0, NULL);

        reset_threads();
        drwrap_redirect_execution(wrapcxt);
    }
}

static void
init_locks()
{
    int i;

    for (i = 0; i < LOCK_COUNT; i++)
        locks[i] = (app_pc) (LOCK_BASE + (i * LOCK_SIZE));
}

DR_EXPORT void
dr_client_main(client_id_t id, int argc, const char *argv[])
{
    /* We need 2 reg slots beyond drreg's eflags slots => 3 slots */
    drreg_options_t ops = {sizeof(ops), 3, false};
    /* Specify priority relative to other instrumentation operations: */
    drmgr_priority_t priority = {
        sizeof(priority),  /* size of struct */
        "memtrace",        /* name of our operation */
        NULL,              /* optional name of operation we should precede */
        NULL,              /* optional name of operation we should follow */
        501};              /* numeric priority: follow dr_wrap (at 500),
                            * otherwise the first mem ref gets lost.
                            */
    dr_set_client_name("Shadow memory tutorial client", "");

    page_size = dr_page_size();
    if (!drmgr_init())
        DR_ASSERT(false);
    if (!drwrap_init())
        DR_ASSERT(false);
    if (drsym_init(0) != DRSYM_SUCCESS)
        DR_ASSERT(false);
    if (!drutil_init())
        DR_ASSERT(false);

    client_id = id;
    mutex = dr_mutex_create();

    /* register events */
    dr_register_exit_event(event_exit);
    if (!drmgr_register_thread_init_event(event_thread_init) ||
        !drmgr_register_thread_exit_event(event_thread_exit) ||
        !drmgr_register_bb_app2app_event(event_bb_app2app, &priority) ||
        !drmgr_register_bb_instrumentation_event(NULL, event_bb_insert, &priority) ||
        !drmgr_register_bb_app2app_event(event_app2app, NULL) ||
        drreg_init(&ops) != DRREG_SUCCESS) {
        /* something is wrong: can't continue */
        DR_ASSERT(false);
        return;
    }

    module_data_t *main_module = dr_get_main_module();
    main_module_base = (uint64_t) main_module->start;
    app_kernel_function = (app_pc) (main_module_base + KERNEL_OFFSET);
    dr_free_module_data(main_module);

    drwrap_wrap(app_kernel_function, pre_write, post_write);

    tls_index = drmgr_register_tls_field();
    DR_ASSERT(tls_index != -1);

    init_locks();

    code_cache_init();
    /* make it easy to tell, by looking at log file, which client executed */
    dr_log(NULL, LOG_ALL, 1, "Client 'memtrace' initializing\n");
#ifdef SHOW_RESULTS
    if (dr_is_notify_on()) {
# ifdef WINDOWS
        /* ask for best-effort printing to cmd window.  must be called at init. */
        dr_enable_console_printing();
# endif
        dr_fprintf(STDERR, "Client memtrace is running\n");
    }
#endif
}

/*
 * instrument_mem is called whenever a memory reference is identified.
 * It inserts code before the memory reference to to fill the memory buffer
 * and jump to our own code cache to call the write_and_flush_buffer when the buffer is full.
 */
static void
instrument_mem(void *drcontext, instrlist_t *ilist, instr_t *where,
               int pos, bool is_write)
{
    instr_t *instr, *call, *restore;
    opnd_t   ref, opnd1, opnd2;
    reg_id_t reg1, reg2;
    drvector_t allowed;
    per_thread_t *data;
    app_pc pc;

    data = (per_thread_t *) drmgr_get_tls_field(drcontext, tls_index);

    /* Steal two scratch registers.
     * reg2 must be ECX or RCX for jecxz.
     */
    drreg_init_and_fill_vector(&allowed, false);
    drreg_set_vector_entry(&allowed, DR_REG_XCX, true);
    if (drreg_reserve_register(drcontext, ilist, where, &allowed, &reg2) !=
        DRREG_SUCCESS ||
        drreg_reserve_register(drcontext, ilist, where, NULL, &reg1) != DRREG_SUCCESS) {
        DR_ASSERT(false); /* cannot recover */
        drvector_delete(&allowed);
        return;
    }
    drvector_delete(&allowed);

    if (is_write)
       ref = instr_get_dst(where, pos);
    else
       ref = instr_get_src(where, pos);

    /* use drutil to get mem address */
    drutil_insert_get_mem_addr(drcontext, ilist, where, ref, reg1, reg2);

    /* The following assembly performs the following instructions
     * mem_buf_ptr->is_write = is_write;
     * mem_buf_ptr->addr  = addr;
     * mem_buf_ptr->size  = size;
     * mem_buf_ptr->pc    = pc;
     * mem_buf_ptr++;
     * if (mem_buf_ptr >= buf_end_ptr)
     *    write_and_flush_buffer();
     */
    drmgr_insert_read_tls_field(drcontext, tls_index, ilist, where, reg2);
    /* Load data->mem_buf_ptr into reg2 */
    opnd1 = opnd_create_reg(reg2);
    opnd2 = OPND_CREATE_MEMPTR(reg2, offsetof(per_thread_t, mem_buf_ptr));
    instr = INSTR_CREATE_mov_ld(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);

    /* Move write/read to is_write field */
    opnd1 = OPND_CREATE_MEM32(reg2, offsetof(mem_ref_t, is_write));
    opnd2 = OPND_CREATE_INT32(is_write);
    instr = INSTR_CREATE_mov_imm(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);

    /* Store address in memory ref */
    opnd1 = OPND_CREATE_MEMPTR(reg2, offsetof(mem_ref_t, addr));
    opnd2 = opnd_create_reg(reg1);
    instr = INSTR_CREATE_mov_st(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);

    /* Store size in memory ref */
    opnd1 = OPND_CREATE_MEMPTR(reg2, offsetof(mem_ref_t, size));
    /* drutil_opnd_mem_size_in_bytes handles OP_enter */
    opnd2 = OPND_CREATE_INT32(drutil_opnd_mem_size_in_bytes(ref, where));
    instr = INSTR_CREATE_mov_st(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);

    /* Store pc in memory ref */
    pc = instr_get_app_pc(where);
    /* For 64-bit, we can't use a 64-bit immediate so we split pc into two halves.
     * We could alternatively load it into reg1 and then store reg1.
     * We use a convenience routine that does the two-step store for us.
     */
    opnd1 = OPND_CREATE_MEMPTR(reg2, offsetof(mem_ref_t, pc));
    instrlist_insert_mov_immed_ptrsz(drcontext, (ptr_int_t) pc, opnd1,
                                     ilist, where, NULL, NULL);

    /* Increment reg value by pointer size using lea instr */
    opnd1 = opnd_create_reg(reg2);
    opnd2 = opnd_create_base_disp(reg2, DR_REG_NULL, 0,
                                  sizeof(mem_ref_t),
                                  OPSZ_lea);
    instr = INSTR_CREATE_lea(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);

    /* Update the data->mem_buf_ptr */
    drmgr_insert_read_tls_field(drcontext, tls_index, ilist, where, reg1);
    opnd1 = OPND_CREATE_MEMPTR(reg1, offsetof(per_thread_t, mem_buf_ptr));
    opnd2 = opnd_create_reg(reg2);
    instr = INSTR_CREATE_mov_st(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);

    /* we use lea + jecxz trick for better performance
     * lea and jecxz won't disturb the eflags, so we won't insert
     * code to save and restore application's eflags.
     */
    /* lea [reg2 - buf_end] => reg2 */
    opnd1 = opnd_create_reg(reg1);
    opnd2 = OPND_CREATE_MEMPTR(reg1, offsetof(per_thread_t, buf_end));
    instr = INSTR_CREATE_mov_ld(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);
    opnd1 = opnd_create_reg(reg2);
    opnd2 = opnd_create_base_disp(reg1, reg2, 1, 0, OPSZ_lea);
    instr = INSTR_CREATE_lea(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);

    /* jecxz call */
    call  = INSTR_CREATE_label(drcontext);
    opnd1 = opnd_create_instr(call);
    instr = INSTR_CREATE_jecxz(drcontext, opnd1);
    instrlist_meta_preinsert(ilist, where, instr);

    /* jump restore to skip clean call */
    restore = INSTR_CREATE_label(drcontext);
    opnd1 = opnd_create_instr(restore);
    instr = INSTR_CREATE_jmp(drcontext, opnd1);
    instrlist_meta_preinsert(ilist, where, instr);

    /* clean call */
    /* We jump to lean procedure which performs full context switch and
     * clean call invocation. This is to reduce the code cache size.
     */
    instrlist_meta_preinsert(ilist, where, call);
    /* mov restore DR_REG_XCX */
    opnd1 = opnd_create_reg(reg2);
    /* this is the return address for jumping back from lean procedure */
    opnd2 = opnd_create_instr(restore);
    /* We could use instrlist_insert_mov_instr_addr(), but with a register
     * destination we know we can use a 64-bit immediate.
     */
    instr = INSTR_CREATE_mov_imm(drcontext, opnd1, opnd2);
    instrlist_meta_preinsert(ilist, where, instr);
    /* jmp code_cache */
    opnd1 = opnd_create_pc(code_cache);
    instr = INSTR_CREATE_jmp(drcontext, opnd1);
    instrlist_meta_preinsert(ilist, where, instr);

    /* Restore scratch registers */
    instrlist_meta_preinsert(ilist, where, restore);
    if (drreg_unreserve_register(drcontext, ilist, where, reg1) != DRREG_SUCCESS ||
        drreg_unreserve_register(drcontext, ilist, where, reg2) != DRREG_SUCCESS)
        DR_ASSERT(false);
}
